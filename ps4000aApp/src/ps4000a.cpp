/* ps4000a.cpp
 *
 * This is a driver for a PicoScope 4000A Series area detector.
 *
 * Author: Hinko Kocevar
 *         European Spallation Source ERIC
 *
 * Created: Dec 19, 2017
 *
 * Based on ps4000con.[ch] from picosdk-c-examples:
 * Copyright (C) 2009 - 2017 Pico Technology Ltd. See PICO_LICENSE.md file for terms.
 *
 * EPICS areaDetector driver:
 * Copyright (C) 2018 - 2020 European Spallation Source ERIC
 */

#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>

#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>

#include <dbAccess.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsStdio.h>
#include <epicsMutex.h>
#include <cantProceed.h>
#include <epicsExit.h>
#include <iocsh.h>

#include "asynNDArrayDriver.h"
#include <epicsExport.h>

#include "ps4000a.h"

static const char *driverName = "PS4000A";

#define PREF4

// debug and other messages
#define __M(p, x) { \
    printf("%s %s:%s:%d ", p, driverName, __func__, __LINE__); \
    x; \
}

#define E(x) __M("[ERR]", x)
#define I(x) __M("[INF]", x)
#define D(x) __M("[DBG]", x)
// for verbose debugging (enabled when compiled for linux-x86_64-debug target)
#if _DBG == 1
#define D1(x) __M("[DBG]", x)
#else
#define D1(x)
#endif

int32_t g_inputRanges [PS4000A_MAX_RANGES] = {
                                                10,         // 10 mV
                                                20,         // 20 mV
                                                50,         // 50 mV
                                                100,        // 100 mV
                                                200,        // 200 mV
                                                500,        // 500 mV
                                                1000,       // 1 V
                                                2000,       // 2 V
                                                5000,       // 5 V
                                                10000,      // 10 V
                                                20000,      // 20 V
                                                50000,      // 50 V
                                                100000,     // 100 V
                                                200000};    // 200 V

static void acqTaskC(void *drvPvt) {
    PS4000A *pPvt = (PS4000A *)drvPvt;
    pPvt->acqTask();
}

/**
 * Exit handler, delete the PS4000A object.
 */
static void exitHandler(void *drvPvt) {
    PS4000A *pPvt = (PS4000A *) drvPvt;
    delete pPvt;
}

/** Constructor for PS4000A; most parameters are simply passed to asynNDArrayDriver::asynNDArrayDriver.
  * After calling the base class constructor this method creates a thread to acquire detector data,
  * and sets reasonable default values for parameters defined in this class.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] numSamples The maximum number of samples to collect.
  * \param[in] dataType The initial data type (NDDataType_t) of the traces that this driver will create.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
PS4000A::PS4000A(const char *portName, int numSamples, NDDataType_t dataType,
                               int maxBuffers, size_t maxMemory, int priority, int stackSize)

    : asynNDArrayDriver(portName, MAX_CHANNELS, maxBuffers, maxMemory,
               /* No interfaces beyond those set in ADDriver.cpp */
               asynInt32Mask | asynFloat64Mask | asynOctetMask | asynGenericPointerMask | asynDrvUserMask,
               asynInt32Mask | asynFloat64Mask | asynOctetMask | asynGenericPointerMask,
               ASYN_CANBLOCK | ASYN_MULTIDEVICE, /* asyn flags*/
               1,                                /* autoConnect=1 */
               priority, stackSize),
    mUniqueId(0), mAcquiring(0),
    mHandle(0), mBufferSize(0), mModel(MODEL_NONE),
    mFirstRange(PS4000A_10MV), mLastRange(PS4000A_50V),
    mSignalGenerator(SIGGEN_NONE), mHasETS(false),
    mChannelCount(0), mMaxADCValue(0), mDoDeviceSetup(false)
{
    /* Create an EPICS exit handler */
    epicsAtExit(exitHandler, this);

    /* Create the epicsEvents for signaling to the simulate task when acquisition starts and stops */
    this->mStartEventId = epicsEventCreate(epicsEventEmpty);
    if (!this->mStartEventId) {
        E(printf("epicsEventCreate failure for start event\n"));
        return;
    }
    this->mStopEventId = epicsEventCreate(epicsEventEmpty);
    if (!this->mStopEventId) {
        E(printf("epicsEventCreate failure for stop event\n"));
        return;
    }

    // unit parameters
    createParam(PS4000AStatusString,                                asynParamInt32, &P_Status);
    createParam(PS4000AStatusMessageString,                         asynParamOctet, &P_StatusMessage);

    createParam(PS4000ATimeBase_,                                   asynParamInt32, &P_TimeBase);
    createParam(PS4000ASamplingFrequency_,                          asynParamFloat64, &P_SamplingFreq);
    createParam(PS4000ATickValue_,                                  asynParamFloat64, &P_TickValue);

    createParam(PS4000ANumPreTrigSamples_,                          asynParamInt32, &P_NumPreTrigSamples);
    createParam(PS4000ANumPostTrigSamples_,                         asynParamInt32, &P_NumPostTrigSamples);
    createParam(PS4000ANumTotalSamples_,                            asynParamInt32, &P_NumTotalSamples);
    createParam(PS4000ANumMaxSamples_,                              asynParamInt32, &P_NumMaxSamples);

    createParam(PS4000AMaximumValue_,                               asynParamInt32, &P_MaximumValue);
    createParam(PS4000AMinimumValue_,                               asynParamInt32, &P_MinimumValue);
    createParam(PS4000AMaximumSegments_,                            asynParamInt32, &P_MaximumSegments);

    createParam(PS4000ATrigMode_,                                   asynParamInt32, &P_TrigMode);
    createParam(PS4000ATrigRepeat_,                                 asynParamInt32, &P_TrigRepeat);
    createParam(PS4000ATrigEnabled_,                                asynParamInt32, &P_TrigEnabled);
    createParam(PS4000APulseWidthQualifierEnabled_,                 asynParamInt32, &P_PulseWidthQualifierEnabled);

    // channel specific parameters
    createParam(PS4000AChName_,                                     asynParamOctet, &P_ChName);
    createParam(PS4000AChEnabled_,                                  asynParamInt32, &P_ChEnabled);
    createParam(PS4000AChCoupling_,                                 asynParamInt32, &P_ChCoupling);
    createParam(PS4000AChRange_,                                    asynParamInt32, &P_ChRange);
    createParam(PS4000AChConvert_,                                  asynParamInt32, &P_ChConvert);
    createParam(PS4000AChAnalogOffset_,                             asynParamFloat64, &P_ChAnalogOffset);
    // channel triggering
    createParam(PS4000AChTrigUse_,                                  asynParamInt32, &P_ChTrigUse);
    // simple triggering
    createParam(PS4000AChTrigSimpleThreshold_,                      asynParamInt32, &P_ChTrigSimpleThreshold);
    createParam(PS4000AChTrigSimpleThresholdMv_,                    asynParamInt32, &P_ChTrigSimpleThresholdMv);
    createParam(PS4000AChTrigSimpleDirection_,                      asynParamInt32, &P_ChTrigSimpleDirection);
    createParam(PS4000AChTrigSimpleDelay_,                          asynParamInt32, &P_ChTrigSimpleDelay);
    createParam(PS4000AChTrigSimpleTimeout_,                        asynParamInt32, &P_ChTrigSimpleTimeout);
    // advanced triggering
    createParam(PS4000AChTrigAdvancedCondition_,                    asynParamInt32, &P_ChTrigAdvancedCondition);
    createParam(PS4000AChTrigAdvancedDirection_,                    asynParamInt32, &P_ChTrigAdvancedDirection);
    createParam(PS4000AChTrigAdvancedThresholdUpper_,               asynParamInt32, &P_ChTrigAdvancedThresholdUpper);
    createParam(PS4000AChTrigAdvancedThresholdUpperHysteresis_,     asynParamInt32, &P_ChTrigAdvancedThresholdUpperHysteresis);
    createParam(PS4000AChTrigAdvancedThresholdLower_,               asynParamInt32, &P_ChTrigAdvancedThresholdLower);
    createParam(PS4000AChTrigAdvancedThresholdLowerHysteresis_,     asynParamInt32, &P_ChTrigAdvancedThresholdLowerHysteresis);
    createParam(PS4000AChTrigAdvancedThresholdMode_,                asynParamInt32, &P_ChTrigAdvancedThresholdMode);
    createParam(PS4000AChTrigAdvancedDelay_,                        asynParamInt32, &P_ChTrigAdvancedDelay);
    createParam(PS4000AChTrigAdvancedTimeout_,                      asynParamInt32, &P_ChTrigAdvancedTimeout);
    // pulse width qualifier
    createParam(PS4000AChPulseWidthQualifierCondition_,             asynParamInt32, &P_ChPulseWidthQualifierCondition);
    createParam(PS4000AChPulseWidthQualifierDirection_,             asynParamInt32, &P_ChPulseWidthQualifierDirection);
    createParam(PS4000AChPulseWidthQualifierLimitLower_,            asynParamInt32, &P_ChPulseWidthQualifierLimitLower);
    createParam(PS4000AChPulseWidthQualifierLimitUpper_,            asynParamInt32, &P_ChPulseWidthQualifierLimitUpper);
    createParam(PS4000AChPulseWidthQualifierType_,                  asynParamInt32, &P_ChPulseWidthQualifierType);

    int status = asynSuccess;
    char versionString[20];
    epicsSnprintf(versionString, sizeof(versionString), "%d.%d.%d",
                  DRIVER_VERSION, DRIVER_REVISION, DRIVER_MODIFICATION);
    status |= setStringParam(NDDriverVersion, versionString);
    status |= setIntegerParam(P_Status, 0);
    status |= setStringParam(P_StatusMessage, "Idle");

    status |= setIntegerParam(NDDataType, dataType);
    status |= setIntegerParam(P_NumMaxSamples, numSamples);
    char chName[2] = {0};
    for (int ch = 0; ch < MAX_CHANNELS; ch++) {
        chName[0] = 'A' + ch;
        status |= setStringParam(ch, P_ChName, chName);
        mBuffer[ch] = NULL;
        this->pArrays[ch] = NULL;
        status |= setIntegerParam(ch, P_ChEnabled, 0);
        status |= setIntegerParam(ch, P_ChCoupling, PS4000A_DC);
        status |= setIntegerParam(ch, P_ChRange, PS4000A_5V);
        status |= setIntegerParam(ch, P_ChConvert, 0);
        status |=  setDoubleParam(ch, P_ChAnalogOffset, 0.0);
    }

    if (status) {
        E(printf("unable to set parameters\n"));
        return;
    }

    /* Create the thread that updates the images */
    status = (epicsThreadCreate("PS4000ATask",
                                epicsThreadPriorityMedium,
                                epicsThreadGetStackSize(epicsThreadStackMedium),
                                (EPICSTHREADFUNC)acqTaskC,
                                this) == NULL);
    if (status) {
        E(printf("epicsThreadCreate failure for PS4000A task\n"));
        return;
    }

    I(printf("PicoScope 4000A Series Driver\n"));

    status = checkForDevice();
}

PS4000A::~PS4000A() {
    D1(printf("Shutdown and freeing up memory...\n"));

    // XXX: Need to stop the data thread!!!
    this->lock();
    D1(printf("Data thread is already down!\n"));

    ps4000aCloseUnit(mHandle);
    for (int i = 0; i < MAX_CHANNELS; i++) {
        if (mBuffer[i]) {
            free(mBuffer[i]);
            mBuffer[i] = NULL;
        }
    }

    this->unlock();
    D1(printf("Shutdown complete!\n"));
}

void PS4000A::getInfo() {
    int8_t description [11][25]= { "Driver Version",
                                    "USB Version",
                                    "Hardware Version",
                                    "Variant Info",
                                    "Serial",
                                    "Cal Date",
                                    "Kernel Version",
                                    "Digital HW Version",
                                    "Analogue HW Version",
                                    "Firmware 1",
                                    "Firmware 2"};

    int16_t i, r = 0;
    int8_t line[80];
    char firmware[80];
    int32_t variant = MODEL_NONE;
    PICO_STATUS status = PICO_OK;

    setStringParam(ADManufacturer, "Pico Technology");

    memset(firmware, 0, sizeof(firmware));
    if (mHandle)
    {
        I(printf("device information:\n"));
        for (i = 0; i < 11; i++)
        {
            status = ps4000aGetUnitInfo(mHandle, line, sizeof(line), &r, i);
            if (status == PICO_INVALID_INFO) {
                continue;
            } else if (status) {
                E(printf("failed to get status: %d\n", status));
                return;
            }

            I(printf("  %s: %s\n", description[i], line));

            switch (i) {
            case PICO_DRIVER_VERSION:
                setStringParam(ADSDKVersion, (char *)line);
                break;
            case PICO_USB_VERSION:
                break;
            case PICO_HARDWARE_VERSION:
                break;
            case PICO_VARIANT_INFO:
                setStringParam(ADModel, (char *)line);
                variant = atoi((char *)line);
                break;
            case PICO_BATCH_AND_SERIAL:
                setStringParam(ADSerialNumber, (char *)line);
                break;
            case PICO_CAL_DATE:
                break;
            case PICO_KERNEL_VERSION:
                break;
            case PICO_DIGITAL_HARDWARE_VERSION:
            case PICO_ANALOGUE_HARDWARE_VERSION:
                // ignore Analog and Digital H/W
                break;
            case PICO_FIRMWARE_VERSION_1:
                sprintf(firmware, "%s", (char *)line);
                break;
            case PICO_FIRMWARE_VERSION_2:
                r = strlen(firmware);
                sprintf(firmware+r, ", %s", (char *)line);
                setStringParam(ADFirmwareVersion, firmware);
                break;
            case PICO_MAC_ADDRESS:
                break;
            case PICO_SHADOW_CAL:
                break;
            case PICO_IPP_VERSION:
                break;
            case PICO_DRIVER_PATH:
                break;
            default:
                break;
            }
        }

        switch (variant)
        {
        case MODEL_PS4824:
            mModel                = MODEL_PS4824;
            mSignalGenerator      = SIGGEN_AWG;
            mHasETS               = false;
            mFirstRange           = PS4000A_10MV;
            mLastRange            = PS4000A_50V;
            mChannelCount         = OCTO_SCOPE;
            break;

        case MODEL_PS4225:
            mModel                = MODEL_PS4225;
            mSignalGenerator      = SIGGEN_NONE;
            mHasETS               = false;
            mFirstRange           = PS4000A_50MV;
            mLastRange            = PS4000A_200V;
            mChannelCount         = DUAL_SCOPE;
            break;

        case MODEL_PS4425:
            mModel                = MODEL_PS4425;
            mSignalGenerator      = SIGGEN_NONE;
            mHasETS               = false;
            mFirstRange           = PS4000A_50MV;
            mLastRange            = PS4000A_200V;
            mChannelCount         = QUAD_SCOPE;
            break;

        case MODEL_PS4444:
            mModel                = MODEL_PS4444;
            mSignalGenerator      = SIGGEN_NONE;
            mHasETS               = false;
            mFirstRange           = PS4000A_10MV;
            mLastRange            = PS4000A_50V;
            mChannelCount         = QUAD_SCOPE;
            break;

        default:
            return;
            break;
            }

        ps4000aMaximumValue(mHandle, &mMaxADCValue);
        uint32_t maxSegments;
        ps4000aGetMaxSegments(mHandle, &maxSegments);
        setIntegerParam(P_MaximumSegments, maxSegments);
    }
}

/****************************************************************************
* Callback
* used by ps4000 data block collection calls, on receipt of data.
* used to set global flags etc checked by user routines
****************************************************************************/
void PREF4 CallBackBlock(int16_t handle, PICO_STATUS status, void * pParameter) {
    PS4000A *pPvt = (PS4000A *)pParameter;

    // flag to say done reading data
    pPvt->mDataReady = true;
}

/****************************************************************************
* adc_to_mv
*
* Convert an 16-bit ADC count into millivolts
****************************************************************************/
int32_t PS4000A::adcToMv(int32_t raw, int32_t ch) {
    return (raw * g_inputRanges[ch]) / mMaxADCValue;
}

/****************************************************************************
* mv_to_adc
*
* Convert a millivolt value into a 16-bit ADC count
*
*  (useful for setting trigger thresholds)
****************************************************************************/
int32_t PS4000A::mvToAdc(int32_t mv, int32_t ch) {
    return ((mv * mMaxADCValue ) / g_inputRanges[ch]);
}

asynStatus PS4000A::checkForDevice() {
    PICO_STATUS status;

    // if there is a device handle use it
    if (mHandle > 0) {
        D1(printf("device ALREADY opened, handle %d!\n", mHandle));

        status = ps4000aPingUnit(mHandle);
        D1(printf("ps4000aPingUnit() returned %d\n", status));
        if (status) {
            ps4000aStop(mHandle);
            ps4000aCloseUnit(mHandle);
            mHandle = 0;
            mBufferSize = 0;
            setIntegerParam(P_Status, 9);
            setStringParam(P_StatusMessage, "Not connected!");
            // fall through and try to re-open the device
        } else {
            setIntegerParam(P_Status, 0);
            setStringParam(P_StatusMessage, "Connected");
            return asynSuccess;
        }
    }

    assert(mHandle <= 0);

    D1(printf("opening the device...\n"));
    status = ps4000aOpenUnit(&mHandle, NULL);
    D1(printf("ps4000OpenUnit() returned %d\n", status));
    D1(printf("got handle %d\n", mHandle));

    if (status == PICO_USB3_0_DEVICE_NON_USB3_0_PORT) {
        // USB 3.0 device on non-USB 3.0 port; try to activate USB power then.
        D1(printf("device is on non-USB 3.0 port...\n"));
        // Tell the driver that's ok
        status = ps4000aChangePowerSource(mHandle, PICO_USB3_0_DEVICE_NON_USB3_0_PORT);
    }

    if (status != PICO_OK && status != PICO_EEPROM_CORRUPT) {
        mBufferSize = 0;
        setIntegerParam(P_Status, 9);
        setStringParam(P_StatusMessage, "Not connected!");
        return asynError;
    }
    D1(printf("device opened successfully!\n"));

    // get device info
    getInfo();

    if(mHasETS) {
        // turn off ETS
        status = ps4000aSetEts(mHandle, PS4000A_ETS_OFF, 0, 0, NULL);
        D1(printf("ps4000aSetEts() returned %d\n", status));
    }

    setIntegerParam(P_Status, 0);
    setStringParam(P_StatusMessage, "Connected");
    return asynSuccess;
}

asynStatus PS4000A::setupBuffer(int ch, int numSamples) {
    if (mBuffer[ch] && (mBufferSize == (size_t)numSamples)) {
        D1(printf("ch %d, have raw buffer AND same buffer size %d\n", ch, numSamples));
        return asynSuccess;
    }

    // free previous buffers and allocate new buffers for
    // unit (uncoverted) data in ADC counts
    if (mBuffer[ch]) {
        free(mBuffer[ch]);
        mBuffer[ch] = NULL;
    }

    D1(printf("ch %d, NEW raw bufffer size %d\n", ch, numSamples));
    mBuffer[ch] = (int16_t *)malloc(numSamples * sizeof(int16_t));
    PICO_STATUS status = ps4000aSetDataBuffer(mHandle,
                                              (PS4000A_CHANNEL)ch,
                                              mBuffer[ch],
                                              numSamples,
                                              0,
                                              PS4000A_RATIO_MODE_NONE);
    // D1(printf("ps4000aSetDataBuffer() returned %d, ch %d\n", status, ch));
    if (status) {
        E(printf("ps4000aSetDataBuffer() returned %d, ch %d\n", status, ch));
        return asynError;
    }

    return asynSuccess;
}

asynStatus PS4000A::setupChannel(int ch) {
    assert(ch >= PS4000A_CHANNEL_A);
    assert(ch <= PS4000A_CHANNEL_H);

    int enabled;
    getIntegerParam(ch, P_ChEnabled, &enabled);
    int coupling;
    getIntegerParam(ch, P_ChCoupling, &coupling);
    int range;
    getIntegerParam(ch, P_ChRange, &range);
    double offset;
    getDoubleParam(ch, P_ChAnalogOffset, &offset);

    D1(printf("ch %d, enabled %d, coupling %s, range %d\n",
            ch, enabled,
            (coupling == PS4000A_AC) ? "AC" : "DC",
            range));

    // set unit channel parameters
    PICO_STATUS status = ps4000aSetChannel(mHandle,
                                           (PS4000A_CHANNEL)ch,
                                           enabled,
                                           (PS4000A_COUPLING)coupling,
                                           (PICO_CONNECT_PROBE_RANGE)range,
                                           offset);
    // D1(printf("ps4000SetChannel() returned %d\n", status));
    if (status) {
        E(printf("ps4000SetChannel() returned %d\n", status));
        return asynError;
    }
    return asynSuccess;
}

asynStatus PS4000A::setupTriggerNone() {
    // disable or enable triggering on a single channel
    // will disable any previously enabled triggering (on this or any other channel)
    // only one channel can be used as trigger source in this (simple) mode
    PICO_STATUS status = ps4000aSetSimpleTrigger(mHandle,
                                                 0,
                                                 (PS4000A_CHANNEL)0,
                                                 0,
                                                 (PS4000A_THRESHOLD_DIRECTION)0,
                                                 0,
                                                 0);

    // D1(printf("ps4000aSetSimpleTrigger() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetSimpleTrigger() returned %d\n", status));
        return asynError;
    }
    D1(printf("trigger mode NONE!\n"));

    return asynSuccess;
}

asynStatus PS4000A::setupTriggerSimple(int ch, int trigUse) {
    // if conversion for channel is enabled, then take user value in mV
    // otherwise take user value in ADC counts
    int convert;
    getIntegerParam(ch, P_ChConvert, &convert);
    int threshold;
    if (convert) {
        int range;
        getIntegerParam(ch, P_ChRange, &range);
        getIntegerParam(ch, P_ChTrigSimpleThresholdMv, &threshold);
        threshold = mvToAdc(threshold, range);
    } else {
        getIntegerParam(ch, P_ChTrigSimpleThreshold, &threshold);
    }
    int direction;
    getIntegerParam(ch, P_ChTrigSimpleDirection, &direction);
    int delay;
    getIntegerParam(ch, P_ChTrigSimpleDelay, &delay);
    int timeout;
    getIntegerParam(ch, P_ChTrigSimpleTimeout, &timeout);
    // disable or enable triggering on a single channel
    // will disable any previously enabled triggering (on this or any other channel)
    // only one channel can be used as trigger source in this (simple) mode
    PICO_STATUS status = ps4000aSetSimpleTrigger(mHandle,
                                                 trigUse,
                                                 (PS4000A_CHANNEL)ch,
                                                 threshold,
                                                 (PS4000A_THRESHOLD_DIRECTION)direction,
                                                 delay,
                                                 timeout);

    // D1(printf("ps4000aSetSimpleTrigger() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetSimpleTrigger() returned %d\n", status));
        return asynError;
    }
    D1(printf("ch %d, trigger mode SIMPLE, use %d!\n", ch, trigUse));

    return asynSuccess;
}

asynStatus PS4000A::setupTriggerAdvanced(int ch, int trigUse) {
    // FIXME: should there be PVs for thresholds in mV (similar to simple
    //        triggering above)?
    int upThr, upThrHyst;
    getIntegerParam(ch, P_ChTrigAdvancedThresholdUpper, &upThr);
    getIntegerParam(ch, P_ChTrigAdvancedThresholdUpperHysteresis, &upThrHyst);
    int loThr, loThrHyst;
    getIntegerParam(ch, P_ChTrigAdvancedThresholdLower, &loThr);
    getIntegerParam(ch, P_ChTrigAdvancedThresholdLowerHysteresis, &loThrHyst);
    int thrMode;
    getIntegerParam(ch, P_ChTrigAdvancedThresholdMode, &thrMode);
    // see PS4000a API for struct definition
    PS4000A_TRIGGER_CHANNEL_PROPERTIES channelProperties = {
        (int16_t)upThr,
        (uint16_t)upThrHyst,
        (int16_t)loThr,
        (uint16_t)loThrHyst,
        (PS4000A_CHANNEL)ch,
        (PS4000A_THRESHOLD_MODE)thrMode
    };
    int nChannelProperties = 1;
    int auxOutputEnabled = 0;
    int timeout;
    getIntegerParam(ch, P_ChTrigAdvancedTimeout, &timeout);
    D1(printf("ch %d trigger properties UP %d, %d DOWN %d %d, mode %d\n",
        ch, upThr, upThrHyst, loThr, loThrHyst, thrMode));
    PICO_STATUS status = ps4000aSetTriggerChannelProperties(mHandle,
                                                            &channelProperties,
                                                            nChannelProperties,
                                                            auxOutputEnabled,
                                                            timeout);
    // D1(printf("ps4000aSetTriggerChannelProperties() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetTriggerChannelProperties() returned %d\n", status));
        return asynError;
    }

    int condition;
    getIntegerParam(ch, P_ChTrigAdvancedCondition, &condition);
    // condition can be one of: True, False, Don't care
    // is Don't case is applied, then the triggering as setup for this channel
    // should be ignored
    PS4000A_CONDITION triggerConditions = {
        (PS4000A_CHANNEL)ch,
        (PS4000A_TRIGGER_STATE)condition
    };
    int nTriggerConditions = 1;
    // clear previous trigger condition
    PS4000A_CONDITIONS_INFO info = (PS4000A_CONDITIONS_INFO)(PS4000A_CLEAR);
    if (trigUse) {
        // clear previous trigger condition and add new trigger condition
        info = (PS4000A_CONDITIONS_INFO)(PS4000A_CLEAR | PS4000A_ADD);
    }
    D1(printf("ch %d trigger nconditions %d, condition %d, info %d\n",
        ch, nTriggerConditions, triggerConditions.condition, info));
    status = ps4000aSetTriggerChannelConditions(mHandle,
                                                &triggerConditions,
                                                nTriggerConditions,
                                                info);
    // D1(printf("ps4000aSetTriggerChannelConditions() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetTriggerChannelConditions() returned %d\n", status));
        return asynError;
    }

    int direction;
    getIntegerParam(ch, P_ChTrigAdvancedDirection, &direction);
    PS4000A_DIRECTION directions = {
        (PS4000A_CHANNEL)ch,
        (PS4000A_THRESHOLD_DIRECTION)direction
    };
    int nDirections = 1;
    D1(printf("ch %d trigger ndirections %d, direction %d\n",
        ch, nDirections, directions.direction));
    status = ps4000aSetTriggerChannelDirections(mHandle,
                                                &directions,
                                                nDirections);
    // D1(printf("ps4000aSetTriggerChannelDirections() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetTriggerChannelDirections() returned %d\n", status));
        return asynError;
    }

    int delay;
    getIntegerParam(ch, P_ChTrigAdvancedDelay, &delay);
    D1(printf("ch %d trigger delay %d\n", ch, delay));
    status = ps4000aSetTriggerDelay(mHandle, delay);
    // D1(printf("ps4000aSetTriggerDelay() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetTriggerDelay() returned %d\n", status));
        return asynError;
    }

    // pulse width qualifier setup
    int pwqDirection;
    getIntegerParam(ch, P_ChPulseWidthQualifierDirection, &pwqDirection);
    int pwqLimitLower, pwqLimitUpper;
    getIntegerParam(ch, P_ChPulseWidthQualifierLimitLower, &pwqLimitLower);
    getIntegerParam(ch, P_ChPulseWidthQualifierLimitUpper, &pwqLimitUpper);
    int pwqType;
    getIntegerParam(ch, P_ChPulseWidthQualifierType, &pwqType);
    D1(printf("ch %d pwq direction %d, lower %d, upper %d, type %d\n",
        ch, pwqDirection, pwqLimitLower, pwqLimitUpper, pwqType));

    status = ps4000aSetPulseWidthQualifierProperties(mHandle,
                                                     (PS4000A_THRESHOLD_DIRECTION)pwqDirection,
                                                     pwqLimitLower,
                                                     pwqLimitUpper,
                                                     (PS4000A_PULSE_WIDTH_TYPE)pwqType);
    // D1(printf("ps4000aSetPulseWidthQualifierProperties() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetPulseWidthQualifierProperties() returned %d\n", status));
        return asynError;
    }

    int pwqCondition;
    getIntegerParam(ch, P_ChPulseWidthQualifierCondition, &pwqCondition);
    PS4000A_CONDITION pwqConditions = {
        (PS4000A_CHANNEL)ch,
        (PS4000A_TRIGGER_STATE)pwqCondition
    };
    int pwqNConditions = 1;
    // clear previous pulse width qualifier condition
    PS4000A_CONDITIONS_INFO pwqInfo = (PS4000A_CONDITIONS_INFO)(PS4000A_CLEAR);
    if (trigUse) {
        // clear previous pulse width qualifier condition and add new pulse width qualifier condition
        pwqInfo = (PS4000A_CONDITIONS_INFO)(PS4000A_CLEAR | PS4000A_ADD);
    }
    D1(printf("ch %d pwq nconditions %d, condition %d, info %d\n",
        ch, pwqNConditions, pwqConditions.condition, pwqInfo));
    status = ps4000aSetPulseWidthQualifierConditions(mHandle,
                                                     &pwqConditions,
                                                     pwqNConditions,
                                                     pwqInfo);
    // D1(printf("ps4000aSetPulseWidthQualifierConditions() returned %d\n", status));
    if (status) {
        E(printf("ps4000aSetPulseWidthQualifierConditions() returned %d\n", status));
        return asynError;
    }

    D1(printf("ch %d, trigger mode ADVANCED, use %d!\n", ch, trigUse));

    return asynSuccess;
}

asynStatus PS4000A::setupTrigger(int ch) {
    // unit wide selector for triggering mode; none, simple or advanced
    int trigMode;
    getIntegerParam(P_TrigMode, &trigMode);
    // D1(printf("unit wide triggering mode %d!\n", trigMode));

    // channel triggering selector
    //  - if ignored, then do not touch any trigger settings fot this channel
    //  - if used, then setup triggering as per users choice
    int trigUse;
    getIntegerParam(ch, P_ChTrigUse, &trigUse);
    if (! trigUse) {
        D1(printf("ch %d, trigger mode %d, trigger NOT used!\n", ch, trigMode));
        return asynSuccess;
    }

    asynStatus status = asynSuccess;
    if (trigMode == 1) {
        // simple triggering setup
        status = setupTriggerSimple(ch, trigUse);
    } else if (trigMode == 2) {
        // advanced triggering setup
        status = setupTriggerAdvanced(ch, trigUse);
    }
    return status;

}

asynStatus PS4000A::setupTimeBase() {
    PICO_STATUS status;

    int totalSamples;
    getIntegerParam(P_NumTotalSamples, &totalSamples);
    double samplingFreq;
    getDoubleParam(P_SamplingFreq, &samplingFreq);
    D1(printf("using sampling frequency %g\n", samplingFreq));
    double tick = 1.0 / samplingFreq;
    D1(printf("want tick %g s\n", tick));
    // get the inital time base value, see PS4000A API doc
    unsigned int timeBase = (unsigned int) (tick / (1.0 / 80.0e6)) - 1;
    D1(printf("initial time base %d\n", timeBase));
    assert(timeBase >= 0);
    // default value
    setDoubleParam(P_TickValue, 1.0);

    /*
     * Find the maximum number of samples, and the time interval (in nanoseconds),
     * at the current timebase if it is valid.
     * If the timebase index is not valid, increment by 1 and try again.
     */
    float timeIntervalNs;
    int maxSamples;
    while ((status = ps4000aGetTimebase2(mHandle, (unsigned int)timeBase, totalSamples, &timeIntervalNs, &maxSamples, 0))) {
        timeBase++;
        D1(printf("new timebase %d, totalSamples %d, maxSamples %d, timeInterval %.1f ns (status %d)\n",
                  timeBase, totalSamples, maxSamples, timeIntervalNs, status));
        if (status == PICO_INVALID_CHANNEL) {
            E(printf("at least one channel needs to be enabled!\n"));
            setIntegerParam(P_Status, 10);
            setStringParam(P_StatusMessage, "Enable at least one channel!");
            return asynError;
        }
    }
    D1(printf("final timebase %d, timeInterval %.1f ns (totalSamples %d, maxSamples %d)\n",
              timeBase, timeIntervalNs, totalSamples, maxSamples));

    if (status) {
        E(printf("ps4000GetTimebase() returned %d\n", status));
        return asynError;
    }

    setIntegerParam(P_TimeBase, timeBase);
    tick = (double)timeIntervalNs / 1.0e9;
    setDoubleParam(P_TickValue, tick);
    D1(printf("final tick %g s\n", tick));

    return asynSuccess;
}

asynStatus PS4000A::setupDevice() {
    D1(printf("started..!\n"));

    // total number of samples that user requested (pre-trigger + post-trigger)
    int totalSamples;
    getIntegerParam(P_NumTotalSamples, &totalSamples);

    // always reset all the triggering settings first!
    asynStatus status = setupTriggerNone();

    for (int ch = 0; ch < mChannelCount; ch++) {
        status = setupChannel(ch);
        if (status) {
            return status;
        }

        // enable/disable the triggerring per-channel
        status = setupTrigger(ch);
        if (status) {
            return status;
        }

        // update raw buffer sizes, only if the number of samples has changed
        status = setupBuffer(ch, totalSamples);
        if (status) {
            return status;
        }
    }

    // update the sample count
    mBufferSize = totalSamples;

    status = setupTimeBase();
    if (status) {
        return status;
    }

    // get current min / max ADC count (XXX: what is this used for?)
    int16_t maxValue, minValue;
    ps4000aMaximumValue(mHandle, &maxValue);
    setIntegerParam(P_MaximumValue, (int)maxValue);
    ps4000aMinimumValue(mHandle, &minValue);
    setIntegerParam(P_MinimumValue, (int)minValue);

    // get trigger and pulse width qualifier status
    int16_t triggerEnabled, pulseWidthQualifierEnabled;
    ps4000aIsTriggerOrPulseWidthQualifierEnabled(mHandle,
                                                 &triggerEnabled,
                                                 &pulseWidthQualifierEnabled);
    setIntegerParam(P_TrigEnabled, triggerEnabled);
    setIntegerParam(P_PulseWidthQualifierEnabled, pulseWidthQualifierEnabled);

    if (status == asynSuccess) {
        setIntegerParam(P_Status, 0);
        setStringParam(P_StatusMessage, "Idle");
    }

    D1(printf("finished..!\n"));

    return status;
}

int PS4000A::countEnabledChannels() {
    int enabled;
    int count = 0;
    for (int ch = 0; ch < mChannelCount; ch++) {
        getIntegerParam(ch, P_ChEnabled, &enabled);
        count += enabled;
    }
    D1(printf("have %d enabled channels..\n", count));
    return count;
}

asynStatus PS4000A::armDevice() {
    PICO_STATUS status;

    int timeBase;
    getIntegerParam(P_TimeBase, &timeBase);
    int numPreTrigSamples = 0;
    getIntegerParam(P_NumPreTrigSamples, &numPreTrigSamples);
    int numPostTrigSamples = 0;
    getIntegerParam(P_NumPostTrigSamples, &numPostTrigSamples);
    int numTotalSamples;
    getIntegerParam(P_NumTotalSamples, &numTotalSamples);
    int numMaxSamples;
    getIntegerParam(P_NumMaxSamples, &numMaxSamples);
    D1(printf("samples %d max, total %d, %d pre-trig, %d post-trig..\n",
        numMaxSamples, numTotalSamples,
        numPreTrigSamples, numPostTrigSamples));

    // setup collection, then wait for completion - see CallBackBlock()
    mDataReady = false;
    status = ps4000aRunBlock(mHandle,
                             numPreTrigSamples,
                             numPostTrigSamples,
                             (unsigned int)timeBase,
                             NULL,
                             0,
                             CallBackBlock,
                             (void *)this);

    // D1(printf("ps4000aRunBlock() returned %d\n", status));
    if (status) {
        E(printf("ps4000aRunBlock() returned %d\n", status));
        return asynError;
    }

    return asynSuccess;
}

asynStatus PS4000A::waitDevice() {
    int trigEnabled;
    getIntegerParam(P_TrigEnabled, &trigEnabled);
    if (trigEnabled) {
        D1(printf("wait for trigger, then collect..\n"));
    } else {
        D1(printf("immediate collect..\n"));
    }

    // mDataReady is set to true in CallBackBlock()
    // we can reach this point only if in the acquiring state
    while (! mDataReady) {
        // we need to stop the acquisition before we apply new settings
        if (mDoDeviceSetup) {
            D1(printf("device setup requested!\n"));
            return asynSuccess;
        }

        // user could have stopped the acquisition while we are waiting for trigger
        // epicsEventStatus status = epicsEventTryWait(this->mStopEventId);
        // wait for 1 us max..
        this->unlock();
        usleep(1);
        epicsEventStatus status = epicsEventWaitWithTimeout(this->mStopEventId, 0.0000001);
        this->lock();
        if (status == epicsEventWaitOK) {
            E(printf("acquisition stopped!\n"));
            return asynError;
        }
    }
    assert(mDataReady != false);

    D1(printf("acquisition performed..\n"));
    return asynSuccess;
}

/** Template function to compute the simulated detector data for any data type */
template <typename epicsType> asynStatus PS4000A::updateArraysT() {
    unsigned int gotSamples;
    // int timeBase;
    NDDataType_t dataType;
    epicsType *pData;
    PICO_STATUS status;

    getIntegerParam(NDDataType, (int *)&dataType);

    // we should have data availble for readout
    status = ps4000aGetValues(mHandle,
                              0,
                              (uint32_t *)&gotSamples,
                              0,
                              PS4000A_RATIO_MODE_NONE,
                              0,
                              NULL);
    // D1(printf("ps4000aGetValues() returned %d\n", status));
    if (status) {
        E(printf("ps4000aGetValues() returned %d\n", status));
        return asynError;
    }

    int numTotalSamples;
    getIntegerParam(P_NumTotalSamples, &numTotalSamples);
    D1(printf("we got %d samples (wanted %d)\n", gotSamples, numTotalSamples));
    // XXX: what if it differs??
    assert((unsigned int)numTotalSamples == gotSamples);
    // mSampleCount = gotSamples;

    // each trace in its own array
    size_t dims[1];
    dims[0] = gotSamples;

    int enabled;
    int convert;
    int range;
    for (int ch = 0; ch < mChannelCount; ch++) {
        getIntegerParam(ch, P_ChEnabled, &enabled);
        if (! enabled) {
            D1(printf("ch %d is DISABLED!\n", ch));
        } else {
            D1(printf("ch %d is enabled..\n", ch));

            if (this->pArrays[ch]) {
                this->pArrays[ch]->release();
                this->pArrays[ch] = NULL;
            }
            this->pArrays[ch] = pNDArrayPool->alloc(1, dims, dataType, 0, 0);
            pData = (epicsType *)this->pArrays[ch]->pData;
            memset(pData, 0, gotSamples * sizeof(epicsType));

            getIntegerParam(ch, P_ChConvert, &convert);
            getIntegerParam(ch, P_ChRange, &range);

            for (unsigned int o = 0; o < gotSamples; o++) {
                if (convert) {
                    pData[o] = (epicsType) adcToMv(mBuffer[ch][o], range);
                } else {
                    pData[o] = (epicsType) mBuffer[ch][o];
                }
            }
#if _DBG == 1
            D1(printf("ch %d data (%s): ", ch, convert?"mV":"ADC"));
            for (unsigned int o = 0; o < 10; o++) {
                printf("%d ", (int)pData[o]);
            }
            printf("\n");
#endif
        }
    }

    D1(printf("samples collected..\n"));
    return asynSuccess;
}

/** Get the new scope data */
asynStatus PS4000A::updateArrays() {
    int dataType;
    getIntegerParam(NDDataType, &dataType);

    switch (dataType) {
        case NDInt8:
            return updateArraysT<epicsInt8>();
            break;
        case NDUInt8:
            return updateArraysT<epicsUInt8>();
            break;
        case NDInt16:
            return updateArraysT<epicsInt16>();
            break;
        case NDUInt16:
            return updateArraysT<epicsUInt16>();
            break;
        case NDInt32:
            return updateArraysT<epicsInt32>();
            break;
        case NDUInt32:
            return updateArraysT<epicsUInt32>();
            break;
        case NDFloat32:
            return updateArraysT<epicsFloat32>();
            break;
        case NDFloat64:
            return updateArraysT<epicsFloat64>();
            break;
    }

    return asynError;
}

void PS4000A::setAcquire(int value)
{
    if (value && !mAcquiring) {
        /* Send an event to wake up the simulation task */
        D1(printf("staring data task..\n"));
        epicsEventSignal(this->mStartEventId);
    }
    if (!value && mAcquiring) {
        /* This was a command to stop acquisition */
        /* Send the stop event */
        D1(printf("stopping data task..\n"));
        epicsEventSignal(this->mStopEventId);
    }
}

void PS4000A::acqTask()
{
    int status;
    NDArray *pTrace;
    epicsTimeStamp startTime;
    int arrayCounter;
    int chEnabled;
    int trigRepeat;
    const char *functionName = "acqTask";

    while (! interruptAccept) {
        epicsThreadSleep(0.1);
    }

    this->lock();
    /* Loop forever */
    while (1) {
        /* Has acquisition been stopped? */
        status = epicsEventTryWait(this->mStopEventId);
        if (status == epicsEventWaitOK) {
            D1(printf("user stopped the acq..\n"));
            mAcquiring = 0;
        }

        /* If we are not acquiring then wait for a semaphore that is given when acquisition is started */
        if (! mAcquiring) {
            asynPrint(this->pasynUserSelf, ASYN_TRACE_FLOW,
                "%s:%s: mAcquiring == 0! waiting for acquire to start\n", driverName, functionName);
            D1(printf("acquire stopped\n"));
            setIntegerParam(ADAcquire, 0);
            callParamCallbacks();

            // release the lock while we wait for an event
            // that says acquire has started, then lock again
            this->unlock();
            status = epicsEventWait(this->mStartEventId);
            this->lock();
            D1(printf("acquire started\n"));
            // at this point user has started the accquisition

            // we want to acquire now..
            mAcquiring = 1;
            setIntegerParam(ADAcquire, 1);
            callParamCallbacks();
        }

        // see if the device is present, try to connect if not
        status = checkForDevice();
        if (status) {
            mAcquiring = 0;
            continue;
        }

        if (mDoDeviceSetup) {
            ps4000aStop(mHandle);

            // device setup needs to be performed because some parameters
            // have changed
            status = setupDevice();
            if (status) {
                mAcquiring = 0;
            } else {
                mDoDeviceSetup = false;
                // at least one channel must be enabled to enter the
                // acquiring state
                if (countEnabledChannels()) {
                    mAcquiring = 1;
                }
            }
        }
        if (! mAcquiring) {
            continue;
        }

        status = armDevice();
        if (status) {
            mAcquiring = 0;
            continue;
        }

        status = waitDevice();
        if (status) {
            ps4000aStop(mHandle);
            mAcquiring = 0;
            continue;
        }

        if (mDoDeviceSetup) {
            ps4000aStop(mHandle);

            // device setup needs to be performed because some parameters
            // have changed
            status = setupDevice();
            if (status) {
                mAcquiring = 0;
            } else {
                mDoDeviceSetup = false;
                // at least one channel must be enabled to enter the
                // acquiring state
                if (countEnabledChannels()) {
                    mAcquiring = 1;
                }
            }

            // do not continue
            continue;
        }

        this->unlock();

        // collect the data from the unit and update the individual
        // channel NDArrays
        status = updateArrays();

        this->lock();

        if (status) {
            mAcquiring = 0;
            continue;
        }

        /* Put the frame number and time stamp into the buffer */
        mUniqueId++;
        getIntegerParam(NDArrayCounter, &arrayCounter);
        arrayCounter++;
        setIntegerParam(NDArrayCounter, arrayCounter);
        epicsTimeGetCurrent(&startTime);

        for (int ch = 0; ch < mChannelCount; ch++) {
            getIntegerParam(ch, P_ChEnabled, &chEnabled);
            if ((! chEnabled) || (this->pArrays[ch] == NULL)) {
                continue;
            }
            D1(printf("ch %d enabled, pArray OK, update!\n", ch));
            pTrace = this->pArrays[ch];
            pTrace->uniqueId = mUniqueId;
            pTrace->timeStamp = startTime.secPastEpoch + startTime.nsec / 1.e9;
            updateTimeStamp(&pTrace->epicsTS);
            /* Get any attributes that have been defined for this driver */
            this->getAttributes(pTrace->pAttributeList);
            /* Call the NDArray callback */
            doCallbacksGenericPointer(pTrace, NDArrayData, ch);
            /* Call the callbacks to update any changes */
            callParamCallbacks(ch);
        }
        callParamCallbacks();

        getIntegerParam(P_TrigRepeat, &trigRepeat);
        if (! trigRepeat) {
            D1(printf("trigger repeat not set!\n"));
            mAcquiring = 0;
        }

    }
}

asynStatus PS4000A::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int addr;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    const char *name = NULL;
    getParamName(addr, function, &name);

    /* Set the parameter and readback in the parameter library.  This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setIntegerParam(addr, function, value);

    if (function == ADAcquire) {
        setAcquire(value);
    } else if (function == P_NumPreTrigSamples) {
        int total, post, max;
        getIntegerParam(addr, P_NumPostTrigSamples, &post);
        getIntegerParam(addr, P_NumMaxSamples, &max);
        total = value + post;
        setIntegerParam(addr, P_NumTotalSamples, total);
        // XXX: adjust the total/pre/post if total > max
        assert(total <= max);
        D1(printf("ch %d, param %s (%d) val %d: needs mDoDeviceSetup = true!\n", addr, name, function, value));
        mDoDeviceSetup = true;
    } else if (function == P_NumPostTrigSamples) {
        int total, pre, max;
        getIntegerParam(addr, P_NumPreTrigSamples, &pre);
        getIntegerParam(addr, P_NumMaxSamples, &max);
        total = value + pre;
        setIntegerParam(addr, P_NumTotalSamples, total);
        // XXX: adjust the total/pre/post if total > max
        assert(total <= max);
        D1(printf("ch %d, param %s (%d) val %d: needs mDoDeviceSetup = true!\n", addr, name, function, value));
        mDoDeviceSetup = true;
    } else if ( (function == P_TrigMode)
             || ((function >= P_ChEnabled) && (function <= P_ChPulseWidthQualifierType)) ) {
        D1(printf("ch %d, param %s (%d) val %d: needs mDoDeviceSetup = true!\n", addr, name, function, value));
        mDoDeviceSetup = true;
    } else {
        /* If this parameter belongs to a base class call its method */
        if (function < FIRST_PS4000A_PARAM) {
            status = asynNDArrayDriver::writeInt32(pasynUser, value);
        }
    }

    /* Do callbacks so higher layers see any changes */
    callParamCallbacks(addr);

    if (status)
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:writeInt32 error, status=%d function=%d, value=%d\n",
              driverName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:writeInt32: function=%d, value=%d\n",
              driverName, function, value);
    return status;
}

asynStatus PS4000A::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    int addr;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);
    const char *name = NULL;
    getParamName(addr, function, &name);

    /* Set the parameter and readback in the parameter library.  This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setDoubleParam(addr, function, value);

    if (function == P_SamplingFreq) {
        D1(printf("ch %d, param %s (%d) val %g: needs mDoDeviceSetup = true!\n", addr, name, function, value));
        mDoDeviceSetup = true;
    } else {
        /* If this parameter belongs to a base class call its method */
        if (function < FIRST_PS4000A_PARAM) {
            status = asynNDArrayDriver::writeFloat64(pasynUser, value);
        }
    }

    /* Do callbacks so higher layers see any changes */
    callParamCallbacks(addr);

    if (status)
        asynPrint(pasynUser, ASYN_TRACE_ERROR,
              "%s:writeFloat64 error, status=%d function=%d, value=%f\n",
              driverName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:writeFloat64: function=%d, value=%f\n",
              driverName, function, value);
    return status;
}

/** Report status of the driver.
  * Prints details about the driver if details>0.
  * It then calls the ADDriver::report() method.
  * \param[in] fp File pointed passed by caller where the output is written to.
  * \param[in] details If >0 then driver details are printed.
  */
void PS4000A::report(FILE *fp, int details)
{

    fprintf(fp, "PS4000A %s\n", this->portName);
    if (details > 0) {
        int totalSamples, maxSamples, dataType;
        getIntegerParam(P_NumTotalSamples, &totalSamples);
        getIntegerParam(P_NumMaxSamples, &maxSamples);
        getIntegerParam(NDDataType, &dataType);
        fprintf(fp, " total nr. samples:   %d\n", totalSamples);
        fprintf(fp, "  max. nr. samples:   %d\n", maxSamples);
        fprintf(fp, "         data type:   %d\n", dataType);
    }
    /* Invoke the base class method */
    asynNDArrayDriver::report(fp, details);
}

/** Configuration command, called directly or from iocsh */
extern "C" int PS4000AConfig(const char *portName, int numSamples, int dataType,
                                 int maxBuffers, int maxMemory, int priority, int stackSize)
{
    new PS4000A(portName, numSamples, (NDDataType_t)dataType,
                    (maxBuffers < 0) ? 0 : maxBuffers,
                    (maxMemory < 0) ? 0 : maxMemory,
                    priority, stackSize);
    return(asynSuccess);
}

/** Code for iocsh registration */
static const iocshArg initArg0 = {"Port name",     iocshArgString};
static const iocshArg initArg1 = {"# samples",     iocshArgInt};
static const iocshArg initArg2 = {"Data type",     iocshArgInt};
static const iocshArg initArg3 = {"maxBuffers",    iocshArgInt};
static const iocshArg initArg4 = {"maxMemory",     iocshArgInt};
static const iocshArg initArg5 = {"priority",      iocshArgInt};
static const iocshArg initArg6 = {"stackSize",     iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6};
static const iocshFuncDef configPS4000A = {"PS4000AConfig", 7, initArgs};
static void configPS4000ACallFunc(const iocshArgBuf *args)
{
    PS4000AConfig(args[0].sval, args[1].ival, args[2].ival, args[3].ival,
                         args[4].ival, args[5].ival, args[6].ival);
}

static void PS4000ARegister(void)
{
    iocshRegister(&configPS4000A, configPS4000ACallFunc);
}

extern "C" {
epicsExportRegistrar(PS4000ARegister);
}

