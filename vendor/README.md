# Vendor resources


## RPM
  
  https://labs.picotech.com/rpm/x86_64/

## DEB

  https://labs.picotech.com/debian/pool/main/libp/libps4000/


## C example

  https://github.com/picotech/picosdk-c-examples
  https://gitlab.esss.lu.se/beam-diagnostics/misc/picosdk-c-examples/blob/master/ps4000a/ps4000aCon/ps4000aCon.c

